<?php 
/* Template Name:Blog*/ 
include(TEMPLATEPATH . '/indexheader.php');
?>
<?php
/*
Template Name: Search Page
*/
?>
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <h1>
                Search Posts
                </h1>
            </div>
            <div class="col-lg-8 col-sm-8">
                <ol class="breadcrumb pull-right">
                    <li>
                        <a href="#">
                            Home
                        </a>
                    </li>
                    <li class="active">
                  Search Posts
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$search = new WP_Query($search_query);
?>
<?php get_search_form(); ?>

</div>

		<?php get_footer(); ?>