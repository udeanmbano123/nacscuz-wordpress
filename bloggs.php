<?php 
/* Template Name:bloggs*/ 
include(TEMPLATEPATH . '/indexheader.php');
?>
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <h1>
                 Blog
                </h1>
            </div>
            <div class="col-lg-8 col-sm-8">
                <ol class="breadcrumb pull-right">
                    <li>
                        <a href="#">
                            Home
                        </a>
                    </li>
                    <li class="active">
                    Blog
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 
     
            <?php the_content(); ?>

<?php endwhile; ?>
<?php endif; ?>


</div>

		<?php get_footer(); ?>