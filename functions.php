<?php 

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:

		 wp_register_script( 'bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );
	 wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ) );

	 	 wp_register_script( 'modernizr', get_template_directory_uri() . '/js/parallax-slider/modernizr.custom.28468.js', array( 'jquery' ) );
	 wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/parallax-slider/modernizr.custom.28468.js', array( 'jquery' ) );


		 	 wp_register_script( 'jquery-1.8.3', get_template_directory_uri() . '/js/jquery-1.8.3.min.js', array( 'jquery' ) );
	 wp_enqueue_script( 'jquery-1.8.3', get_template_directory_uri() . '/js/jquery-1.8.3.min.js', array( 'jquery' ) );

 
		 	 wp_register_script( 'hover-dropdown', get_template_directory_uri() . '/js/hover-dropdown.js', array( 'jquery' ) );
	 wp_enqueue_script( 'hover-dropdown', get_template_directory_uri() . '/js/hover-dropdown.js', array( 'jquery' ) );


		 	 wp_register_script( 'flexislider', get_template_directory_uri() . '/js/jquery.flexslider.js', array( 'jquery' ) );
	 wp_enqueue_script( 'flexislider', get_template_directory_uri() . '/js/jquery.flexslider.js', array( 'jquery' ) );

   
	 	 wp_register_script( 'bxslider', get_template_directory_uri() . '/assets/bxslider/jquery.bxslider.js', array( 'jquery' ) );
	 wp_enqueue_script( 'bxslider', get_template_directory_uri() . '/assets/bxslider/jquery.bxslider.js', array( 'jquery' ) );


		 	 wp_register_script( 'parallax', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js', array( 'jquery' ) );
	 wp_enqueue_script( 'parallax', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js', array( 'jquery' ) );

  
		 	 wp_register_script( 'wow', get_template_directory_uri() . 'js/wow.min.js', array( 'jquery' ) );
	 wp_enqueue_script( 'wow', get_template_directory_uri() . 'js/wow.min.js', array( 'jquery' ) );

 
	 	 wp_register_script( 'owl', get_template_directory_uri() . '/assets/owlcarousel/owl.carousel.js', array( 'jquery' ) );
	 wp_enqueue_script( 'owl', get_template_directory_uri() . '/assets/owlcarousel/owl.carousel.js', array( 'jquery' ) );


		 	 wp_register_script( 'easing', get_template_directory_uri() . '/js/jquery.easing.min.js', array( 'jquery' ) );
	 wp_enqueue_script( 'easing', get_template_directory_uri() . '/js/jquery.easing.min.js', array( 'jquery' ) );

 
		 	 wp_register_script( 'link-hover', get_template_directory_uri() . '/js/link-hover.js', array( 'jquery' ) );
	 wp_enqueue_script( 'link-hover', get_template_directory_uri() . '/js/link-hover.js', array( 'jquery' ) );

		 	 wp_register_script( 'super-fish', get_template_directory_uri() . '/js/superfish.js', array( 'jquery' ) );
	 wp_enqueue_script( 'super-fish', get_template_directory_uri() . '/js/superfish.js', array( 'jquery' ) );
	
  
		 	 wp_register_script( 'cslider', get_template_directory_uri() . '/js/parallax-slider/jquery.cslider.js', array( 'jquery' ) );
	 wp_enqueue_script( 'cslider', get_template_directory_uri() . '/js/parallax-slider/jquery.cslider.js', array( 'jquery' ) );



	 	 wp_register_script( 'safe', get_template_directory_uri() . '/js/safe.js', array( 'jquery' ) );
	 wp_enqueue_script( 'safe', get_template_directory_uri() . '/js/safe.js', array( 'jquery' ) );


		 	 wp_register_script( 'common-scripts', get_template_directory_uri() . '/js/common-scripts.js', array( 'jquery' ) );
	 wp_enqueue_script( 'common-scripts', get_template_directory_uri() . '/js/common-scripts.js', array( 'jquery' ) );

		 	 wp_register_script( 'safe2', get_template_directory_uri() . '/js/safe2.js', array( 'jquery' ) );
	 wp_enqueue_script( 'safe2', get_template_directory_uri() . '/js/safe2.js', array( 'jquery' ) );

 
		 	 wp_register_script( 'safe3', get_template_directory_uri() . '/js/safe3.js', array( 'jquery' ) );
	 wp_enqueue_script( 'safe3', get_template_directory_uri() . '/js/safe3.js', array( 'jquery' ) );

  
  wp_register_script('googleapis', ("http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"), false, '');
	wp_enqueue_script('googleapis');
		 
  

		 	 wp_register_script( 'map', get_template_directory_uri() . '/js/map.js', array( 'jquery' ) );
	 wp_enqueue_script( 'map', get_template_directory_uri() . '/js/map.js', array( 'jquery' ) );

  
		
    
	 wp_register_script('moder', ("http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js/js/map.js"), false, '');
	wp_enqueue_script('moder');
	
			 	 wp_register_script( 'work', get_template_directory_uri() . '/js/work.js', array( 'jquery' ) );
	 wp_enqueue_script( 'work', get_template_directory_uri() . '/js/work.js', array( 'jquery' ) );


}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );

function contact_send_message() {

    $contact_errors = false;

    // get the posted data
    $name = $_POST["name"];
    $email_address = $_POST["email"];
    $phone_num = $_POST["phone"];
    $message = $_POST["message"];

    // write the email content
    $header .= "MIME-Version: 1.0\n";
    $header .= "Content-Type: text/html; charset=utf-8\n";
    $header .= "From:" . $email_address;

    $message = "Name: $name\n";
    $message .= "Email Address: $email_address\n";
    $message .= "Telefon: $contact_phone\n";
    $message .= "Message:\n$message";

    $subject = "Zpráva z webu";
    $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";

    $to = "udeanmbano@gmail.com";

    // send the email using wp_mail()
    if( !wp_mail($to, $subject, $message, $header) ) {
        $contact_errors = true;
    }

}
add_action('contact_send_message', 'contact_send_message');
?>