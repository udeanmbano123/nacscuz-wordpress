
<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 address wow fadeInUp" data-wow-duration="2s" data-wow-delay=".1s">
                               <h1>
                        SECRETARIAT
                    </h1>
                    <address>
                        <p><i class="fa fa-home pr-10"></i>2nd Floor InterCoop Business Centre Office Suite 210 46 3rd Street </p>
                        <p><i class="fa fa-globe pr-10"></i>Corner 3rd Street & Central Avenue Harare. Zimbabwe </p>
                        <p><i class="fa fa-phone pr-10"></i>TEL: +263772283318. +263712321911 </p>
                        <p><i class="fa fa-envelope pr-10"></i>Email :   <a href="javascript:;">info@nacscuz.coop </a></p>
                    </address>
                    <br />
                    <h1>Strategic Partners
                    </h1>
                 
                        <ul class="page-footer-list">

                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="corpserve">Corpserve</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="mastered">Mastered Seed Consultants</a>
                            </li>
                  

                        </ul>

                    <br />
                    <h1>
                       Other
                    </h1>

                    <ul class="page-footer-list">

                        <li>
                            <i class="fa fa-angle-right"></i>
                            <a href="research">Research and Development</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right"></i>
                            <a href="Blog">Blog</a>
                        </li>

                        <li>
                            <i class="fa fa-angle-right"></i>
                            <a href="broadcast">News</a>
                        </li>
  <li>
                            <i class="fa fa-angle-right"></i>
                            <a href="gallery">Gallery</a>
                        </li>

                    </ul>
                 
                </div>
                <div class="col-lg-3 col-sm-3">
                    <div class="page-footer wow fadeInUp" data-wow-duration="2s" data-wow-delay=".5s">
                        <h1>
                            Our History
                        </h1>
                        <ul class="page-footer-list">
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="transformation">The Transformation</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="strategicvision">Strategic Vision</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="strategicmission">Strategic Mission</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="hyperinflation">Hyperinflation</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="nazcuzrevival">Revival</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="currentchallenges">Current Challenges</a>
                            </li>

                            
                            
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="addresschallenges.php">Addressing Challenges</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="conclusion.php">Conclusion</a>
                            </li>
                          
                        </ul>
                    </div>

					 <h1>
                           Products And Services
                        </h1>
                        <ul class="page-footer-list">
                     
                            <li>
							 <i class="fa fa-angle-right"></i>
                                <a href="Advocacy">Advocacy</a>
                            </li>
                            <li>
							 <i class="fa fa-angle-right"></i>
                                <a href="financial">Nacscuz Central Financial Services</a>
                            </li>
                               <li>
							    <i class="fa fa-angle-right"></i>
                                <a href="risk">Nacscuz SACCOsure  Risk Management Services</a>
                            </li>
                          <li>
						   <i class="fa fa-angle-right"></i>
                                <a href="education">Education And Training Services</a>
                            </li>
							<li>
							 <i class="fa fa-angle-right"></i>
                                <a href="business">Nacscuz  Business Advisory Services</a>
                            </li>
						
                </div>
                <div class="col-lg-3 col-sm-3">
                    <div class="page-footer wow fadeInUp" data-wow-duration="2s" data-wow-delay=".5s">
                        <h1>
                            About Us
                        </h1>
                        <ul class="page-footer-list">
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="About">NAME</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="missionandvision">Mission & Vision</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="coreandgoverning">Core Values</a>
                            </li>

                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="nazmandate">NACSCUZ Mandate</a>
                            </li>
                        </ul>
                    </div>
                    <div class="page-footer wow fadeInUp" data-wow-duration="2s" data-wow-delay=".5s">
                        <h1>
                           Membership
                        </h1>
                        <ul class="page-footer-list">
                        
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="requirements">Requirements</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="application">Application</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="benefits">Benefits</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3">
                    <div class="page-footer wow fadeInUp" data-wow-duration="2s" data-wow-delay=".5s">
                        <h1>
                            Governance
                        </h1>
                        <ul class="page-footer-list">
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="boardmembers">Board Members</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="technicalboard">Technical Advisory</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="secremanage">Secre/Management</a>
                            </li>


                        </ul>
                    </div>

                </div>
                <div class="col-lg-3 col-sm-3">
                    <div class="page-footer wow fadeInUp" data-wow-duration="2s" data-wow-delay=".5s">
                        <h1>
                            Events
                        </h1>
                
                        <ul class="page-footer-list">
								<li>
								  <i class="fa fa-angle-right"></i>
                                <a href="past">Past Events</a>
                            </li>
                            <li>
                                
                                <a class="dropdown-header" style="font-size: 14px;" href="#"><strong>Ongoing events</strong></a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="saccos">SACCO Awareness Campaign Program</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="financiallit">Financial literacy</a>
                            </li>
                            <li>
                                <i class="fa fa-angle-right"></i>
                                <a href="financiallit">Consumer Education</a>
                            </li>
                            
                            <li>

                                <a class="dropdown-header" style="font-size: 14px;" href="#"><strong>Upcoming events</strong></a>
                            </li>
                            <li><i class="fa fa-angle-right"></i><a href="oncoming">Saccos Multistackholder</a></li>


                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->
    <!--small footer start -->
    <footer class="footer-small">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6 pull-right">
                    <ul class="social-link-footer list-unstyled">
                        <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".1s"><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".2s"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                       <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".4s"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".5s"><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="wow flipInX" data-wow-duration="2s" data-wow-delay=".6s"><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="copyright">
                        <p>&copy; Copyright 2016 - <a style="color:white" href="http://www.escrowsystems.net" >Escrow Systems</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<?php wp_footer(); ?>
</body>
</html>