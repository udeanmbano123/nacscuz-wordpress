<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="favicon.ico">

    <title>
       Nazcuz | Addressing Challenges
    </title>

   <!-- Le styles -->
<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">

	
	  <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js">
    </script>
    <script src="js/respond.min.js">
    </script>
    <![endif]-->
</head>

<body>
    <!--header start-->
    <header class="head-section">
        <div class="navbar navbar-default navbar-static-top container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index" style="float: left"><img width="100" height="100" src="http://localhost/wordpress/wp-content/uploads/2016/08/nacscuz-logo.png" /></a>
                <a style="position:relative;font-size:12pt; font-weight:bold" class="navbar-brand" href="~/Home">  NACSCUZ<br /> National Association of Cooperative Savings and Credit Unions of Zimbabwe</a>
 </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="index">
                            Home <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="index#mainoptions">Main</a>
                            </li>
                          
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="#">
                            Governance<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                                <a href="boardmembers">Board Members</a>
                            </li>
                         <li>
                                <a href="technicalboard">Technical Board</a>
                            </li>
                          <li>
                                <a href="secremanage">Secretarial Management</a>
                            </li>
               

                           </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="index.html">
                            Events <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
							   <li>
                                <a  href="past"><strong>Past events</strong></a>
                            </li>
                            <li>
                                <a class="dropdown-header" style="font-size: 14px;" href="#"><strong>Ongoing events</strong></a>
                            </li>
                            <li>
                                <a href="saccos">SACCO Awareness Campaign Program</a>
                            </li>
                            <li>
                                   <a href="financiallit">Financial Literacy Program</a>
                            </li>
                            <li>
                                <a href="financiallit">Consumer Education</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header" style="font-size: 14px;"><strong>Upcoming events</strong></li>
                            <li><a href="oncoming">Saccos Multistackholder Events</a></li>
                          
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="#">
                            Membership <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                 <a href="requirements"> Requirements</a>
                            </li>
                            <li>
                                 <a href="application">Application</a>
                            </li>
                            <li>
                                 <a href="benefits">Benefits</a>
                            </li>
                   
                     
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="#">
                            History <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                           <li>
                          
                                <a href="transformation">The Transformation</a>
                            </li>
                            <li>
                    
                                <a href="strategicvision">Strategic Vision</a>
                            </li>
                            <li>
                          
                                <a href="strategicmission">Strategic Mission</a>
                            </li>
                            <li>
                              
                                <a href="hyperinflation">Hyperinflation</a>
                            </li>
                            <li>
                           
                                <a href="nazcuzrevival">Revival</a>
                            </li>
                            <li>
                   
                                <a href="currentchallenges">Current Challenges</a>
                            </li>

                            
                            
                            <li class="active">
                
                                <a href="addresschallenges">Addressing Challenges</a>
                            </li>
                            <li>
                            
                                <a href="conclusion">Conclusion</a>
                            </li>
                          </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="#">
                            About Us <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                                   <li>
                            
                                <a href="About">NAME</a>
                            </li>
                            <li>
                   
                                <a href="missionandvision">Mission And Vision</a>
                            </li>
                            <li>
              
                                <a href="coreandgoverning">Core Values</a>
                            </li>

                            <li>
                    
                                <a href="nazmandate">NACSCUZ Mandate</a>
                            </li>
                        </ul>
                    </li> <li>
                                <a href="gallery">Gallery</a>
                            </li>
<li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="index.html">
                          Products And Services<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="Advocacy">Advocacy</a>
                            </li>
                            <li>
                                <a href="financial">Nacscuz Central Financial Services</a>
                            </li>
                               <li>
                                <a href="risk">Nacscuz SACCOsure  Risk Management Services</a>
                            </li>
                          <li>
                                <a href="education">Education And Training Services</a>
                            </li>
							 	 	<li>
                                <a href="business">Nacscuz  Business Advisory Services</a>
                            </li>
                        </ul>
                    </li>
					  <li>
                                <a href="research">Research And Development</a>
                            </li>
                    <li><a href="Blog">Blog</a>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover="dropdown" data-toggle="dropdown" href="index.html">
                           Strategic Partners <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="corpserve">Corpserve</a>
                            </li>
                            <li>
                                  <a href="mastered">Mastered Seed Consultants</a>
                            </li>
                         

                        </ul>
                    </li>
                    <li>
                        <a href="broadcast">News</a>
                    </li><li > <a  href="contact">Contacts</a></li>

                    <li>
                        <form id="searchform" class="form-inline" method="get" action="http://www.google.com/search" target="_blank">

                            <div class="form-group">

                          

                                <input class="form-control search" type="text" name="q" size="40" maxlength="255" value="" placeholder="Google Search..."/>
                            </div>
                            <div class="form-group">
                                <input class="btn btn-info" type="submit" value="Go">
                                <input type="hidden" name="sitesearch" value="http://www.nacscuz.coop/" target="_blank">

                            </div>


                        </form>

           
                    </li>
                </ul>
            </div>
        </div>
    </header>
	
